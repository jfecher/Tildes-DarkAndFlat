A dark and flat theme for [tildes](https://tildes.net)

This will *only* restyle the default tildes white theme!
Make sure to have the White (default) theme selected!

See [here](https://gitlab.com/Bauke/styles/wikis/Installing-Styles) for
installation steps.

## Pictures

![main page](https://gitlab.com/jfecher/Tildes-DarkAndFlat/raw/master/img/tildes-main.png)

![comments](https://gitlab.com/jfecher/Tildes-DarkAndFlat/raw/master/img/tildes-comments.png)